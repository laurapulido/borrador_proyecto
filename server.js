var express=require('express'); /*importamos librerias*/
var app=express();              /*iniciando el framework web*/
var port=process.env.PORT || 3000;
var bodyParser=require('body-parser');
app.use(bodyParser.json());

//iniciamos las variables de bbdd
var baseMLabURL = 'https://api.mlab.com/api/1/databases/lpulidol/collections/';
var mLabAPIKey = "apiKey=x9j5e7ravi1Zl6LMT7JgGH6TIAVJQJLd";
var requestJson = require('request-json');

app.listen(port);
console.log("API escuchando en el puerto "+port);

/*************************************************************/
/****************** lista los usuarios que hay ***************/
/*************************************************************/

app.get("/apitechu/v2/users",
    function(req,res){
        console.log("GET /apitechu/v2/users");

        var httpClient = requestJson.createClient (baseMLabURL);

        httpClient.get("user?" + mLabAPIKey,
          function(err,resMLab,body){
            var response = !err ? body :{"msg" : "Error obteniendo usuarios"}
            res.send(response);
          }
      );
    }
);

/*************************************************************/
/************ Muestra el usuario con el id *******************/
/*************************************************************/

app.get("/apitechu/v2/users/:id",
    function(req,res){
        console.log("GET /apitechu/v2/users/:id");

        var httpClient = requestJson.createClient (baseMLabURL);
        var id = req.params.id;
        var query = 'q={"id":' + id + '}';

        httpClient.get("user?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
            var response ={};
            if(err){
              response = {"msg" : "Error obteniendo usuario"}
              res.status(500);
            }else {
              if(body.length>0){
                response = body;
              }else{
                response = {"msg":"Usuario no encontrado"};
                res.status(404);
              }
            }
            res.send(response);
          }
        );
    }
);

/*************************************************************/
/****** Hace login del usuario indicado en el body ***********/
/*************************************************************/

app.put("/apitechu/v2/login",
  function(req,res){
    console.log("PUT /apitechu/v2/login");

    var email=req.body.email;
    var pass=req.body.password;
    var httpClient = requestJson.createClient (baseMLabURL);
    var query = 'q={"email":"' + email + '","password":"' + pass + '"}';
    var putBody ='{"$set":{"logged":"true"}}';

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
          var response ={};
          if(err){
            response = {"msg" : "Error obteniendo usuario"}
            res.status(500);
          }else {
            if(body.length>0){
              //response = {"msg":"Usuario logado","id": body[0].id};
              response = body[0];
              //escribimos parámetro logged=true en el usuario
              httpClient.put("user?s" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err,resMLab2,body2){console.log("login realizado "+ body[0]);});
            }else{
              response = {"msg":"Usuario no encontrado o contraseña incorrecta"};
              res.status(404);
            }
          }
          res.send(response);
        }
    );
  }
);

/*************************************************************/
/****** Hace logout del usuario indicado en el body **********/
/*************************************************************/

app.put("/apitechu/v2/logout",
  function(req,res){
    console.log("PUT /apitechu/v2/logout");

    var id=req.body.id;
    var httpClient = requestJson.createClient (baseMLabURL);
    console.log(id);
    var query = 'q={"id":' + id + '}';
    var putBody='{"$unset":{"logged":""}}';
    console.log("user?" + query + "&" + mLabAPIKey);
    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
          var response ={};
          if(err){
            console.log("error primero");
            response = {"msg" : "Error obteniendo usuario"}
            res.status(500);
          }else {
            if(body.length>0){
              response = {"msg":"Usuario deslogado","id": body[0].id};
              //borramos logged=true en el usuario
              httpClient.put("user?s" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err,resMLab2,body2){console.log("Usuario deslogado "+body);});
            }else{
              console.log("error porque body vacío");
              response = {"msg":"Usuario no encontrado o contraseña incorrecta"};
              res.status(404);
            }
          }
          res.send(response);
      });
    }
);

/*************************************************************/
/*** Lista las cuentas del usuario con el id indicado ********/
/*************************************************************/

app.get("/apitechu/v2/users/:id/accounts",
    function(req,res){
        console.log("GET /apitechu/v2/users/:id/accounts");

        var httpClient = requestJson.createClient (baseMLabURL);
        var userid = req.params.id;
        var query = 'q={"USERID":' + userid + '}';
        console.log("cuentas?" + query + "&" + mLabAPIKey);

        httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
            //console.log("cuentas?" + query + "&" + mLabAPIKey);
            var response ={};

            if(err){
              response = {"msg" : "Error obteniendo cuentas"}
              res.status(500);
            }else {
              if(body.length>0){
                response = body;
              }else{
                response = {"msg":"Cuentas no encontradas"};
                res.status(404);
              }
            }
            res.send(response);
          }
      );
   }
);

/*************************************************************/
/*********** Lista el balance de una cuenta ******************/
/*************************************************************/

app.get("/apitechu/v2/user/:iban/bal",
    function(req,res){
        console.log("GET /apitechu/v2/user/:IBAN/bal");
        var httpClient = requestJson.createClient (baseMLabURL);
        var IBAN = req.params.iban;
        var query = 'q={"IBAN":"' + IBAN + '"}';

        httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
            var response ={};
            if(err){
              response = {"msg" : "Error obteniendo balance"}
              res.status(500);
            }else {
              if(body.length>0){
                console.log("el balance es "+ body[0].BALANCE);
                response = body[0];
              }else{
                response = {"msg":"Balance no encontrado"};
                res.status(404);
              }
            }
            res.send(response);
          }
        );
      }
  );

/*************************************************************/
/*********** Lista movimientos de una cuenta *****************/
/*************************************************************/

app.get("/apitechu/v2/user/:iban/mov",
    function(req,res){
        console.log("GET /apitechu/v2/user/:IBAN/mov");
        var httpClient = requestJson.createClient (baseMLabURL);
        var IBAN = req.params.iban;
        var query = 's={"timestamp":-1}&q={"IBAN":"' + IBAN + '"}';

        httpClient.get("movimientos?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
            //console.log("movimientos?" + query + "&" + mLabAPIKey);
            var response ={};
            if(err){
              response = {"msg" : "Error obteniendo movimientos"}
              res.status(500);
            }else {
              if(body.length>0){
                response = body;
              }else{
                response = {"msg":"Movimientos no encontrados"};
                res.status(404);
              }
            }
            res.send(response);
          }
      );
    }
);

/*************************************************************/
/*********** Hago un ingreso en una cuenta *******************/
/*************************************************************/

app.post("/apitechu/v2/user/:iban/ingreso",
  function(req,res){
    console.log("POST /apitechu/v2/user/:iban/ingreso");
    var httpClient = requestJson.createClient(baseMLabURL);
    var iban = req.params.iban;
    var query = 'q={"iban":"' + iban + '"}';

    //obtengo la hora actual
    var f = new Date();
    var hours=f.getHours();
    if(hours<10) hours="0"+hours;
    var minutes=f.getMinutes();
    if(minutes<10) minutes="0"+minutes;
    var ahora=f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear() + " " + hours + ":" + minutes;
    var cantidad=parseInt(req.body.cantidad);
    if(req.body.tipo=="Reintegro") {
      console.log("se trata de un reintegro");
      cantidad=-cantidad;
    }
    var data={
      "timestamp" : Math.floor(Date.now() / 1000),
      "IBAN": iban,
      "fecha" : ahora,
      "tipo": req.body.tipo,
      "cantidad": cantidad,
      "concepto": req.body.concepto
    };
    var jsonUserData=JSON.stringify(data);
    console.log(jsonUserData);
    var insercion=httpClient.post("movimientos?" + mLabAPIKey,JSON.parse(jsonUserData),
      function(err,resMLab,body){
        var response = {"msg" : "Ingreso en bbdd"};
        if(err){
          response = {"msg" : "Error en el ingreso"};
          res.status(500);
        }else {
          //Cambio el saldo de la cuenta
          response= cambiaBalance(cantidad,iban);
        }
        res.send(response);
      }
    );
  }
);

/* Función para cambiar el balance de una cuenta una vez realizada una operación */
function cambiaBalance(cantidad,iban){
  var query = 'q={"IBAN":"' + iban + '"}';
  var httpClient = requestJson.createClient(baseMLabURL);

  httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
  function(err,resMLab,body){
    var response={};
    if(err){
      response = {"msg" : "Error recogiendo balance"}
      res.status(500);
    }else {
      if(body.length>0){
        response=body;
        var balance = parseInt(body[0].BALANCE);
        console.log("balance es "+ balance + " cantidad es "+cantidad);
        //Actualizo el balance con la suma
        balance= balance+cantidad;
        actualizaBalance(balance,iban);
      }else{
        response = {"msg":"Balance no encontrado"};
        res.status(404);
      }
      return response;
    }
  }
  );
}

/* Función para actualizar el balance de una cuenta dado el iban */
function actualizaBalance(balance,iban){
  var httpClient = requestJson.createClient(baseMLabURL);
  var query = 'q={"IBAN":"' + iban + '"}';
  var putBody ='{"$set":{"BALANCE":'+balance+'}}';

  httpClient.put("cuentas?" + query + "&" + mLabAPIKey, JSON.parse(putBody),function(err,resMLab,body){});
}

/*************************************************************/
/*********** Crear un nuevo usuario **************************/
/*************************************************************/

app.post("/apitechu/v2/users/new",function(req,res){
  console.log("POST /apitechu/v2/users/new");

  /** consulta para comprobar si el usuario ya existe*/
  var httpClient = requestJson.createClient(baseMLabURL);
  var email=req.body.email;
  var query = 'q={"email":"' + email + '"}';
  httpClient.get("user?" + query + "&" + mLabAPIKey,function(err,resMLab,body){
    if(err){
      var response = {"msg" : "Error obteniendo email nuevo usuario"}
      res.status(500);
    }else{
      if(body.length>0){
        //si el usuario existe
        var response = {"msg":"El usuario ya existe"};
        res.send(response);
      }else{
        //si el usuario no existe
        var id_nuevo=0;
        //para obtener el id nuevo cojo todos los usuarios que hay y los cuento
        httpClient.get("user?" + mLabAPIKey,function(err,resMLab,body2){
          //el id nuevo que voy a usar es el número de existentes más 1
          id_nuevo=body2.length+1;
          //recopilo los datos para crear el nuevo usuario
          var newUser={
            "id" : id_nuevo,
            "first_name":req.body.first_name,
            "last_name":req.body.last_name,
            "email":req.body.email,
            "password":req.body.password,
          };
          creoNewUser(newUser);
          asocioCuentaNueva(id_nuevo);
          res.send(newUser);
          });
        }
      }
    });
  }
);

/* Función que escribe en bbdd el nuevo usuario*/
function creoNewUser(data){
  httpClient=requestJson.createClient(baseMLabURL);
  var jsonUserData=JSON.stringify(data);
  console.log(jsonUserData);
  httpClient.post("user?" + mLabAPIKey,JSON.parse(jsonUserData),
    function(err,resMLab,body){});
}

/* Función que asocia una nueva cuenta a un nuevo usuario*/
function asocioCuentaNueva(id){
  var iban=creoCuentaNueva();
  var cuentaNueva={
    "IBAN": iban,
    "USERID": id,
    "BALANCE": 0
  };
  httpClient=requestJson.createClient(baseMLabURL);
  var jsonUserData=JSON.stringify(cuentaNueva);
  httpClient.post("cuentas?" + mLabAPIKey,JSON.parse(jsonUserData),
    function(err,resMLab,body){});
}

/* Función que crea una nueva cuenta con números aleatorios*/
function creoCuentaNueva() {
  Number.prototype.pad = function(size) {
    var s = String(this);
    while (s.length < (size || 2)) {s = "0" + s;}
    return s;
  }
  const banco='0182';
  var digitoControlIBAN= Math.floor(Math.random()* (99-1)+1);
  var oficina= Math.floor(Math.random()* (9999-1)+1);
  var digitoControl= Math.floor(Math.random()* (99-1)+1);
  var numeroCuenta= String(new Date().getTime()).substring(3, 13);
  return "ES"+digitoControlIBAN.pad(2)+ " " + banco + " " + oficina.pad(4) + " " + digitoControl.pad(2)
    + numeroCuenta.substring(0,2) + " " + numeroCuenta.substring(2,6) + " " + numeroCuenta.substring(6,10);
}
